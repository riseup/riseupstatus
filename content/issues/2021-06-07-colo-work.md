---
title: colo-work
date: 2021-06-07 22:26:28
resolved: true
resolvedWhen: 2021-06-08 01:00:36
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
section: issue
---

The scheduled work has been completed, all systems seem to be working as expected.

Due to on-going, scheduled work at one of our co-location facilities, there is a potential for some
service interruptions. The expected time for completion of this work is within an hour
