---
title: gateway disruptions
date: 2021-07-02 19:53:01
resolved: true
resolvedWhen: 2021-07-02 20:11:24
severity: resolved
affected:
  - RiseupVPN
section: issue
---

What was originally just a Seattle gateway disruption, turned out to also be a
problem with a few different gateways. The issues have been resolved, and we are
seeing normal connections now.

The Seattle gateway for RiseupVPN is experiencing some issues. You will be
automatically redirected to a different gateway if you try to connect to this
gateway, but it may be a less optimal one. We will update this issue as we know
more.
