---
title: Planned systems maitenance ongoing
date: 2021-06-28 18:15:10
resolved: true
resolvedWhen: 2021-06-28 20:24:10
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

Everything should be working again, thanks for your patience.

Some services will be disrupted for some minutes while we perform systems maintenance. Please be patient.
