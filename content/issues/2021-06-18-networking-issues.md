---
title: networking issues
date: 2021-06-18 22:37:29
resolved: true
resolvedWhen: 2021-06-18 23:41:04
severity: resolved
affected:
  - General
section: issue
---

Upstream has removed their incorrect route, and re-established their BGP session
with us, enabling us to properly announce the routes, and resolve this issue.

We have been in contact with upstream about the issues. It appears they are
announcing a route for one of our networks, when they should not. They are
investigating and will get back to us.

Due to networking issues upstream, one of our netblocks is currently
unavailable. We are working with them to return service to normal, and hope to
have things resolved soon.
