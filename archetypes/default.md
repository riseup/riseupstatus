---
title:
date:
resolved: false
resolvedWhen:
severity:
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

Explain the problem in simple words here. Keep updates on top.
